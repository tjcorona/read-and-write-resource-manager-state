Read and Write Resource Manager State
=====================
A plugin that introduces operations for reading and writing the state
of a resource manager, as well as file menu options for executing
these operations.

Building
========
This plugin requires ParaView, Qt5, SMTK and the smtkPQComponentsExt
plugin to build (as well as their dependencies). Due to CMB's use of
singleton behavior classes, you must link against the same SMTK
libraries as those used in the smtkPQComponentsExt plugin.

License
=======

CMB is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details.

[License.txt]: LICENSE.txt
