set(unit_tests
  TestReadWriteResourceManagerState
)

unit_tests(
  Label "resource_manager_state"
  SOURCES ${unit_tests}
  LIBRARIES smtkCore smtkReadWriteResourceManagerState ${Boost_LIBRARIES}
)
