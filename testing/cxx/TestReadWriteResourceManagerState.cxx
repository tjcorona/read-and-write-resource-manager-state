//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/FileItemDefinition.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/ResourceItemDefinition.h"

#include "smtk/common/Paths.h"
#include "smtk/common/UUIDGenerator.h"
#include "smtk/common/testing/cxx/helpers.h"

#include "smtk/io/Logger.h"

#include "smtk/resource/Component.h"
#include "smtk/resource/DerivedFrom.h"
#include "smtk/resource/Manager.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/Registrar.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

#include "operators/ReadResourceManagerState.h"
#include "operators/Registrar.h"
#include "operators/WriteResourceManagerState.h"

#include "smtk/resource/json/jsonResource.h"

SMTK_THIRDPARTY_PRE_INCLUDE
//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include "nlohmann/json.hpp"
SMTK_THIRDPARTY_POST_INCLUDE

#include <fstream>

namespace
{
std::string writeRoot = SCRATCH_DIR;

class ResourceA : public smtk::resource::DerivedFrom<ResourceA, smtk::resource::Resource>
{
public:
  smtkTypeMacro(ResourceA);
  smtkCreateMacro(ResourceA);
  smtkSharedFromThisMacro(smtk::resource::Resource);

  smtk::resource::ComponentPtr find(const smtk::common::UUID&) const override
  {
    return smtk::resource::ComponentPtr();
  }

  std::function<bool(const smtk::resource::ComponentPtr&)> queryOperation(
    const std::string&) const override
  {
    return [](const smtk::resource::ComponentPtr&) { return true; };
  }

  void visit(smtk::resource::Component::Visitor&) const override {}

protected:
  ResourceA()
    : smtk::resource::DerivedFrom<ResourceA, smtk::resource::Resource>()
  {
  }
};

class ResourceB : public smtk::resource::DerivedFrom<ResourceB, ResourceA>
{
public:
  smtkTypeMacro(ResourceB);
  smtkCreateMacro(ResourceB);
  smtkSharedFromThisMacro(smtk::resource::Resource);

protected:
  ResourceB()
    : smtk::resource::DerivedFrom<ResourceB, ResourceA>()
  {
  }
};

class ResourceC : public smtk::resource::DerivedFrom<ResourceC, ResourceB>
{
public:
  smtkTypeMacro(ResourceC);
  smtkCreateMacro(ResourceC);
  smtkSharedFromThisMacro(smtk::resource::Resource);

protected:
  ResourceC()
    : smtk::resource::DerivedFrom<ResourceC, ResourceB>()
  {
  }
};

class ReadResource : public smtk::operation::Operation
{
public:
  smtkTypeMacro(ReadResource);
  smtkSharedFromThisMacro(smtk::operation::Operation);

  ReadResource() {}
  ~ReadResource() override {}

  smtk::io::Logger& log() const override { return m_logger; }

  Result operateInternal() override;

  virtual Specification createSpecification() override;

private:
  mutable smtk::io::Logger m_logger;

  virtual smtk::resource::Resource::Ptr newResource() = 0;

  smtk::resource::Resource::Ptr readResource(const std::string& filename)
  {
    nlohmann::json j;

    std::ifstream file(filename.c_str());
    file >> j;
    file.close();

    auto resource = std::static_pointer_cast<smtk::resource::Resource>(newResource());
    smtk::resource::from_json(j, resource);

    return resource;
  }
};

ReadResource::Result ReadResource::operateInternal()
{
  auto filename = this->parameters()->findFile("filename");
  auto resource = readResource(filename->value());
  auto result = this->createResult(Outcome::SUCCEEDED);
  result->findResource("resource")->setValue(resource);
  return result;
}

ReadResource::Specification ReadResource::createSpecification()
{
  Specification spec = this->createBaseSpecification();

  smtk::attribute::DefinitionPtr readOpDef = spec->createDefinition("ReadResource", "operation");
  readOpDef->setLabel("read");

  auto fileDef = smtk::attribute::FileItemDefinition::New("filename");
  fileDef->setNumberOfRequiredValues(1);
  fileDef->setShouldExist(true);

  readOpDef->addItemDefinition(fileDef);

  smtk::attribute::DefinitionPtr readResDef = spec->createDefinition("read result", "result");
  readResDef->setLabel("read result");

  smtk::attribute::ResourceItemDefinitionPtr rsrcDef =
    smtk::attribute::ResourceItemDefinition::New("resource");
  rsrcDef->setNumberOfRequiredValues(1);
  rsrcDef->setLockType(smtk::resource::LockType::Read);
  readResDef->addItemDefinition(rsrcDef);

  return spec;
}

class ReadResourceA : public ReadResource
{
public:
  smtkTypeMacro(ReadResourceA);
  smtkCreateMacro(ReadResourceA);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtk::resource::Resource::Ptr newResource() override
  {
    return std::static_pointer_cast<smtk::resource::Resource>(ResourceA::create());
  }
};

class ReadResourceB : public ReadResource
{
public:
  smtkTypeMacro(ReadResourceB);
  smtkCreateMacro(ReadResourceB);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtk::resource::Resource::Ptr newResource() override
  {
    return std::static_pointer_cast<smtk::resource::Resource>(ResourceB::create());
  }
};

class ReadResourceC : public ReadResource
{
public:
  smtkTypeMacro(ReadResourceC);
  smtkCreateMacro(ReadResourceC);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtk::resource::Resource::Ptr newResource() override
  {
    return std::static_pointer_cast<smtk::resource::Resource>(ResourceC::create());
  }
};

class WriteResource : public smtk::operation::Operation
{
public:
  smtkTypeMacro(WriteResource);
  smtkCreateMacro(WriteResource);
  smtkSharedFromThisMacro(smtk::operation::Operation);

  WriteResource() {}
  ~WriteResource() override {}

  smtk::io::Logger& log() const override { return m_logger; }

  Result operateInternal() override;

  virtual Specification createSpecification() override;

private:
  mutable smtk::io::Logger m_logger;

  template <class ResourceType>
  bool writeResource(const smtk::resource::ResourcePtr& resource)
  {
    if (auto rsrc = std::dynamic_pointer_cast<ResourceType>(resource))
    {
      nlohmann::json j = resource;

      std::ofstream file(rsrc->location().c_str());
      file << j;
      file.close();

      return true;
    }

    return false;
  }
};

WriteResource::Result WriteResource::operateInternal()
{
  auto resource = this->parameters()->associations()->objectValue();
  if (auto resourceA = std::dynamic_pointer_cast<ResourceA>(resource))
  {
    return writeResource<ResourceA>(resourceA) ? this->createResult(Outcome::SUCCEEDED)
                                               : this->createResult(Outcome::FAILED);
  }
  else if (auto resourceB = std::dynamic_pointer_cast<ResourceB>(resource))
  {
    return writeResource<ResourceB>(resourceB) ? this->createResult(Outcome::SUCCEEDED)
                                               : this->createResult(Outcome::FAILED);
  }
  else if (auto resourceC = std::dynamic_pointer_cast<ResourceC>(resource))
  {
    return writeResource<ResourceC>(resourceC) ? this->createResult(Outcome::SUCCEEDED)
                                               : this->createResult(Outcome::FAILED);
  }

  return this->createResult(Outcome::FAILED);
}

WriteResource::Specification WriteResource::createSpecification()
{
  Specification spec = this->createBaseSpecification();

  smtk::attribute::DefinitionPtr writeOpDef = spec->createDefinition("WriteResource", "operation");
  writeOpDef->setLabel("write");

  smtk::attribute::ReferenceItemDefinitionPtr assocDef =
    smtk::dynamic_pointer_cast<smtk::attribute::ReferenceItemDefinition>(
      smtk::attribute::ReferenceItemDefinition::New("WriteResourceAssociations"));
  assocDef->setAcceptsEntries("smtk::resource::Resource", "", true);
  writeOpDef->setLocalAssociationRule(assocDef);

  smtk::attribute::DefinitionPtr writeResDef = spec->createDefinition("write result", "result");
  writeResDef->setLabel("write result");

  smtk::attribute::ResourceItemDefinitionPtr rsrcDef =
    smtk::attribute::ResourceItemDefinition::New("resource");
  rsrcDef->setNumberOfRequiredValues(1);
  rsrcDef->setIsOptional(false);
  rsrcDef->setLockType(smtk::resource::LockType::Read);
  writeResDef->addItemDefinition(rsrcDef);

  return spec;
}

class WriteResourceA : public WriteResource
{
public:
  smtkTypeMacro(WriteResourceA);
  smtkCreateMacro(WriteResourceA);
  smtkSharedFromThisMacro(smtk::operation::Operation);
};

class WriteResourceB : public WriteResource
{
public:
  smtkTypeMacro(WriteResourceB);
  smtkCreateMacro(WriteResourceB);
  smtkSharedFromThisMacro(smtk::operation::Operation);
};

class WriteResourceC : public WriteResource
{
public:
  smtkTypeMacro(WriteResourceC);
  smtkCreateMacro(WriteResourceC);
  smtkSharedFromThisMacro(smtk::operation::Operation);
};

void registerToManagers(
  smtk::resource::ManagerPtr& resourceManager, smtk::operation::ManagerPtr& operationManager)
{
  // Register the operation namespace's operations with the operation manager.
  smtk::operation::Registrar::registerTo(operationManager);

  smtk::resource_manager_state::Registrar::registerTo(operationManager);

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  // Register ResourceA, ResourceB, ResourceC
  resourceManager->registerResource<ResourceA>();
  resourceManager->registerResource<ResourceB>();
  resourceManager->registerResource<ResourceC>();

  // Register ReadResource
  operationManager->registerOperation<ReadResourceA>();
  operationManager->registerOperation<ReadResourceB>();
  operationManager->registerOperation<ReadResourceC>();

  // Register ReadResource as the readr for ResourceA, ResourceB, ResourceC
  smtk::operation::ReaderGroup(operationManager).registerOperation<ResourceA, ReadResourceA>();
  smtk::operation::ReaderGroup(operationManager).registerOperation<ResourceB, ReadResourceB>();
  smtk::operation::ReaderGroup(operationManager).registerOperation<ResourceC, ReadResourceC>();

  // Register WriteResource
  operationManager->registerOperation<WriteResourceA>();
  operationManager->registerOperation<WriteResourceB>();
  operationManager->registerOperation<WriteResourceC>();

  // Register WriteResource as the writer for ResourceA, ResourceB, ResourceC
  smtk::operation::WriterGroup(operationManager).registerOperation<ResourceA, WriteResourceA>();
  smtk::operation::WriterGroup(operationManager).registerOperation<ResourceB, WriteResourceB>();
  smtk::operation::WriterGroup(operationManager).registerOperation<ResourceC, WriteResourceC>();
}

smtk::operation::Operation::Result TestWriteResourceManagerState(const std::string& directoryPath)
{
  // Create a resource manager
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();

  // Create an operation manager
  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();

  // Register relevant resources and operations to their respective managers
  registerToManagers(resourceManager, operationManager);

  // Create a new ResourceA, ResourceB, ResourceC type
  auto resourceA = resourceManager->create<ResourceA>();
  auto resourceB = resourceManager->create<ResourceB>();
  auto resourceC = resourceManager->create<ResourceC>();

  // Create a WriteResourceManagerState operation
  auto writeResourceManagerState =
    operationManager->create<smtk::resource_manager_state::WriteResourceManagerState>();

  writeResourceManagerState->parameters()->findDirectory("directory")->setValue(directoryPath);

  // Execute the operation.
  return writeResourceManagerState->operate();
}

smtk::operation::Operation::Result TestReadResourceManagerState(const std::string& directoryPath)
{
  // Create a resource manager
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();

  // Create an operation manager
  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();

  // Register relevant resources and operations to their respective managers
  registerToManagers(resourceManager, operationManager);

  // Create a ReadResourceManagerState operation
  auto readResourceManagerState =
    operationManager->create<smtk::resource_manager_state::ReadResourceManagerState>();

  readResourceManagerState->parameters()->findDirectory("directory")->setValue(directoryPath);

  // Execute the operation.
  return readResourceManagerState->operate();
}
}

int TestReadWriteResourceManagerState(int, char** const)
{
  // Set a directory into which our resources will be written.
  std::string directoryPath(writeRoot);
  directoryPath += "/" + smtk::common::UUID::random().toString();
  smtkTest(smtk::common::Paths::directoryExists(directoryPath) == false,
    "This test requires a new directory path.");

std::cout<<"Directory path: "<<directoryPath<<std::endl;

  auto writeResult = TestWriteResourceManagerState(directoryPath);

  // Test the operation result for success.
  smtkTest(writeResult->findInt("outcome")->value() ==
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED),
    "Write Resource Manager State failed.");

  auto readResult = TestReadResourceManagerState(directoryPath);

  // Test the operation result for success.
  smtkTest(readResult->findInt("outcome")->value() ==
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED),
    "Read Resource Manager State failed.");

  // Iterate over results and compare.
  smtk::attribute::ResourceItem::Ptr written = writeResult->findResource("resource");
  smtk::attribute::ResourceItem::Ptr read = readResult->findResource("resource");

  smtkTest(written->numberOfValues() == read->numberOfValues(),
    "Different number of written and read resources.");

  for (std::size_t i = 0; i < written->numberOfValues(); i++)
  {
    auto writtenResource = written->value(i);
    auto readResource = read->value(i);

    smtkTest(
      writtenResource->id() == readResource->id(), "Ids differ between written and read resource.");
  }

  // Delete the generated directory
  boost::filesystem::remove_all(directoryPath);

  return 0;
}
