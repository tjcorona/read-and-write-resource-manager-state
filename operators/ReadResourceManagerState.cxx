//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "ReadResourceManagerState.h"

#include "ReadResourceManagerState_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"

#include "smtk/common/Paths.h"

#include "smtk/io/Logger.h"

#include "smtk/resource/Manager.h"
#include "smtk/resource/json/jsonResource.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/operators/ReadResource.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include <fstream>

namespace smtk
{
namespace resource_manager_state
{

ReadResourceManagerState::ReadResourceManagerState()
{
}

bool ReadResourceManagerState::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  // To read a resource manager's state, the input directory must have an index
  // file.
  auto directoryItem = this->parameters()->findDirectory("directory");
  std::string indexFile = directoryItem->value() + "/index.txt";
 if (!boost::filesystem::exists(boost::filesystem::path(indexFile.c_str())))
  {
    return false;
  }

  // Also, we must have access to the manager itself.

  if (!this->resourceManager())
  {
    return false;
  }

  // Finally, we must have an operation manager from which we access specific
  // write operations.
  if (m_manager.expired())
  {
    return false;
  }

  return true;
}

smtk::operation::Operation::Result ReadResourceManagerState::operateInternal()
{
  // Access the directory into which we read the managed resources.
  auto directoryItem = this->parameters()->findDirectory("directory");

  // Access the resource manager.
  auto resourceManager = this->resourceManager();
  if (!resourceManager)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "Cannot resolve resource manager.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Construct a result to populate with resources that have been successfully
  // readd.
  auto result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  smtk::attribute::ResourceItem::Ptr read = result->findResource("resource");

  // Construct a ReadResource manager. We create the operation using the
  // operation manager because it requires an operation manager to access write
  // methods for specific resource types.
  auto readResource = m_manager.lock()->create<smtk::operation::ReadResource>();
  auto fileNameItem = readResource->parameters()->findFile("filename");

  // Access the index file describing the state of the serialized resource
  // manager.
  std::string indexFileName = directoryItem->value() + "/index.txt";
  std::ifstream indexFile(indexFileName.c_str());

  // Iterate over the resources in the index file and read them.
  bool firstFile = true;
  for (std::string filename; std::getline(indexFile, filename);)
  {
    // The file item "filename" requires at least one value, so we must set the
    // first value and append the rest.
    if (firstFile)
    {
      fileNameItem->setValue(directoryItem->value() + "/" + filename);
      firstFile = false;
    }
    else
    {
      fileNameItem->appendValue(directoryItem->value() + "/" + filename);
    }
  }
  indexFile.close();

  // Read the resources from disk.
  auto readResult = readResource->operate({});

  if (readResult->findInt("outcome")->value() ==
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    // Upon success, append the resources to the outcome.
    auto readResources = readResult->findResource("resource");
    for (std::size_t i = 0; i < readResources->numberOfValues(); ++i)
    {
      read->appendValue(readResources->value(i));
    }
  }
  else
  {
    // If the resource failed to read, change the result status to failure.
    result->findInt("outcome")->setValue(
      static_cast<int>(smtk::operation::Operation::Outcome::FAILED));
  }

  // Return the result object.
  return result;
}

const char* ReadResourceManagerState::xmlDescription() const
{
  return ReadResourceManagerState_xml;
}
}
}
