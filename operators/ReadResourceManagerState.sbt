<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the model "ReadResourceManagerState" Operation -->
<SMTK_AttributeResource Version="3">

  <Definitions>
    <!-- Operation -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="read resource manager state" Label="Read Resource Manager State" BaseType="operation">
      <BriefDecscription>
        Serialize all managed resources to a directory.
      </BriefDecscription>
      <ItemDefinitions>
        <Directory Name="directory" Label="Resource Manager Directory" ShouldExist="false">
          <BriefDescription>The name of the directory containing the saved manager's resources.</BriefDescription>
        </Directory>
      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(read resource manager state)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" NumberOfRequiredValues="0" Extensible="true"></Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

</SMTK_AttributeResource>
