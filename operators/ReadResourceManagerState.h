//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_resource_manager_state_ReadResourceManagerState_h
#define smtk_resource_manager_state_ReadResourceManagerState_h

#include "operators/Exports.h"

#include "smtk/operation/ResourceManagerOperation.h"

namespace smtk
{
namespace resource_manager_state
{

/// Read the state of the resource manager.
class SMTKREADWRITERESOURCEMANAGERSTATE_EXPORT ReadResourceManagerState :
    public smtk::operation::ResourceManagerOperation
{
public:
  smtkTypeMacro(smtk::resource_manager_state::ReadResourceManagerState);
  smtkSharedPtrCreateMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::ResourceManagerOperation);

  virtual bool ableToOperate() override;

protected:
  ReadResourceManagerState();

  Result operateInternal() override;

  virtual const char* xmlDescription() const override;
};
}
}

#endif
